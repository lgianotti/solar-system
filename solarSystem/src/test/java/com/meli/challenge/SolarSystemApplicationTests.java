package com.meli.challenge;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.meli.challenge.config.ConstantsConfig;
import com.meli.challenge.exception.DayNotFoundException;
import com.meli.challenge.model.Planet;
import com.meli.challenge.model.Sun;
import com.meli.challenge.model.Weather;
import com.meli.challenge.service.SolarSystemService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SolarSystemApplicationTests {
	
	private Planet ferengi;
	private Planet vulcano;
	private Planet betasoide;
	List<Planet> planets;

	@Autowired
	private SolarSystemService solarSystemService;
	
	@Autowired 
	private ConstantsConfig constantsConfig;

	@PostConstruct
	public void loadData() {
		ferengi = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getFerengiSpeed(), constantsConfig.getFerengiRadius(), constantsConfig.getFerengiName());
		vulcano = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getVulcanoSpeed(), constantsConfig.getVulcanoRadius(), constantsConfig.getVulcanoName());
		betasoide = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getBetasoideSpeed(), constantsConfig.getBetasoideRadius(), constantsConfig.getBetasoideName());
		
		planets = new ArrayList<>();
		planets.add(ferengi);
		planets.add(vulcano);
		planets.add(betasoide);

		solarSystemService.saveWeather(planets, constantsConfig.getDaysToIterate());
	}

	@Test
	public void planetExists() {
		assertTrue(ferengi != null && vulcano != null && betasoide != null);
	}

	@Test
	public void planetsInitialAlignment() {
		assertTrue(solarSystemService.areAlign(0, planets));
	}

	@Test
	public void ferengiPositionIsRigth() {
		Point2D.Double ferengiPosition = ferengi.getCoordinates(90);
		assertTrue(ferengiPosition.getX() == 500.0 && ferengiPosition.getY() == 0.0);
	}

	@Test
	public void vulcanoPositionisRigth() {
		Point2D.Double vulcanoPosition = vulcano.getCoordinates(90);
		assertTrue(vulcanoPosition.getX() == -1000.0 && vulcanoPosition.getY() == 0.0);
	}

	@Test
	public void betasoidePosition() {
		Point2D.Double betasoidePosition = betasoide.getCoordinates(90);
		assertTrue(betasoidePosition.getX() == -2000.0 && betasoidePosition.getY() == 0.0);
	}

	@Test
	public void alignDaysWithOutSun() {
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i <= constantsConfig.getDaysToIterate(); i++) {
			if (solarSystemService.areAlign(i, planets) 
					&& !solarSystemService.areAlign(i, planets, new Sun())) {
				days.add(i);
			}
		}
		assertTrue(days.isEmpty());
		
	}

	@Test
	public void alignDayWithSun() {
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i <= constantsConfig.getDaysToIterate(); i++) {
			if (solarSystemService.areAlign(i, planets)) {
				days.add(i);
			}
		}
		assertTrue(isDivisibleBy(90, days, (days.size() / 2) + 1));
	}
	
	private boolean isDivisibleBy( int value, List<Integer> days, int index) {
		return days.get(index) % value == 0;
	}

	@Test
	public void notCointainsTheSunV1() {
		assertFalse(solarSystemService.triangleContainsSun(0, planets));
	}

	@Test
	public void notCointainsTheSunV2() {
		assertFalse(solarSystemService.triangleContainsSun(1, planets));
	}

	@Test
	public void containsTheSun() {
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i < constantsConfig.getDaysToIterate(); i++) {
			if (solarSystemService.triangleContainsSun(i, planets)) {
				days.add(i);
			}
		}
		assertTrue(!days.isEmpty());
	}
	
	@Test
	public void triangleNotNegative() {
		double perimeter = solarSystemService.getPerimeter(89, planets);
		assertTrue(perimeter > 0);
	}
	
	@Test
	public void numberOfPeriodsOfDrought() {
		List<Integer> numberOfPeriodsOfDrought = new ArrayList<>();
		numberOfPeriodsOfDrought = solarSystemService.getnumberOfPeriodsOfDrought(planets, constantsConfig.getDaysToIterate());
		assertTrue(!numberOfPeriodsOfDrought.isEmpty() );
	}
	
	@Test
	public void numberOfRainyDays() {
		List<Integer> numberOfRainyDays = new ArrayList<>();
		numberOfRainyDays = solarSystemService.getRainyDays(planets, constantsConfig.getDaysToIterate());
		assertTrue(!numberOfRainyDays.isEmpty());
	}
	
	@Test
	public void dayOfMaxRainy() {
		int day = solarSystemService.getDayOfMaxRainy(planets, constantsConfig.getDaysToIterate());
		assertTrue(day != 0);
	}
	
	@Test 
	public void optimalCondition() {
		List<Integer> numberOfOptimalCondition = new ArrayList<>();
		numberOfOptimalCondition = solarSystemService.getOptimalConditionsDays(planets, constantsConfig.getDaysToIterate());
		assertTrue(!numberOfOptimalCondition.isEmpty());
	}
	
	@Test
	public void weatherlistNotEmpty() {
		List<Weather> weather = solarSystemService.getWeathers(planets);
		assertTrue(!weather.isEmpty());
	}
	
	@Test
	public void getWeather() throws DayNotFoundException {
		Weather w = solarSystemService.getWeatherByDay(360L);
		assertTrue(w != null);
		
	}
	
	@Test
	public void otherPlanetsAlignWithOutSun() {
		Planet a = new Planet(constantsConfig.getPlanteAAngle(), constantsConfig.getNullSpeed() , constantsConfig.getPlanteARadius(), "test");
		Planet b = new Planet(constantsConfig.getPlanteBAngle(), constantsConfig.getNullSpeed() , constantsConfig.getPlanteBRadius(), "test");
		Planet c = new Planet(constantsConfig.getPlanteCAngle(), constantsConfig.getNullSpeed() , constantsConfig.getPlanteCRadius(), "test");
		
		List<Planet> planetsTest = new ArrayList<>();
		planetsTest.add(a);
		planetsTest.add(b);
		planetsTest.add(c);
		
		assertTrue(solarSystemService.areAlign(0, planets) && !solarSystemService.areAlign(0, planetsTest, new Sun()));
	}

}
