package com.meli.challenge.model;

import java.awt.geom.Point2D;

public interface CelestialBody {
	
	default Point2D.Double getCoordinates(int day) {
		return new Point2D.Double(0.0, 0.0);
	}
}
