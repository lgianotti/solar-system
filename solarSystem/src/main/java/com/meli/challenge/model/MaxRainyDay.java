package com.meli.challenge.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MaxRainyDay {
	@Id
	@GeneratedValue
	private long id;
	private int dayOfMaxRain;

	public int getDayOfMaxRain() {
		return dayOfMaxRain;
	}

	public void setDayOfMaxRain(int dayOfMaxRain) {
		this.dayOfMaxRain = dayOfMaxRain;
	}
}
