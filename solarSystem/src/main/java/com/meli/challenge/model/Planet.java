package com.meli.challenge.model;

import java.awt.geom.Point2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meli.challenge.util.SolarSystemUtil;

public class Planet implements CelestialBody {
	
	private static final Logger logger = LoggerFactory.getLogger(Planet.class);
	
	private int speed;
	private double radius;
	private String name;
	private int initialAngle;
	
	public Planet() {}
	
	public Planet(int initialAngle, int speed, double radius, String name) {
		super();
		this.speed = speed;
		this.radius = radius;
		this.name = name;
		this.initialAngle = initialAngle;
	}
	
	@Override
	public Point2D.Double getCoordinates(int day) {
		int angle = initialAngle + day * speed;
		double y = SolarSystemUtil.round(4, Math.sin(Math.toRadians(angle))).multiply(SolarSystemUtil.round(2, radius)).doubleValue();
		double x = SolarSystemUtil.round(4, Math.cos(Math.toRadians(angle))).multiply(SolarSystemUtil.round(2,radius)).doubleValue();
		Point2D.Double coordinate = new Point2D.Double(x, y);
		logger.info("class:Planet, method:getCoordinates, day:{} coordinate:{}", day, coordinate);
		return coordinate;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getInitialAngle() {
		return initialAngle;
	}

	public void setInitialAngle(int initialAngle) {
		this.initialAngle = initialAngle;
	}

	@Override
	public String toString() {
		return "name:" + name + " speed:" + speed + " radius:" + radius  ;
	}
}
