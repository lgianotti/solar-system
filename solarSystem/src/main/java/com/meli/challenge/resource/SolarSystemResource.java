package com.meli.challenge.resource;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.meli.challenge.dto.WeatherResponseDto;
import com.meli.challenge.exception.DayNotFoundException;
import com.meli.challenge.model.Weather;
import com.meli.challenge.service.SolarSystemService;


@RequestMapping("/solar-system/v1")
@RestController
public class SolarSystemResource {
	
	@Autowired
	private SolarSystemService solarSystemService;

	DozerBeanMapper mapper;
	
	@RequestMapping(value = "/weather", method = RequestMethod.GET)
	public WeatherResponseDto getWeather(@RequestParam("day") long day) {
		WeatherResponseDto dto = new WeatherResponseDto();
		try {
			Weather weather = solarSystemService.getWeatherByDay(day);
			mapper = new DozerBeanMapper();
			mapper.map(weather, dto);
			return dto;
		}catch(DayNotFoundException e) {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Day not found", e); 
		}
	}
	
	@RequestMapping(value = "/drought-periods", method = RequestMethod.GET)
	public WeatherResponseDto getDroughtPeriods() {
		WeatherResponseDto response = new WeatherResponseDto();
		int number = solarSystemService.getnumberOfPeriodsOfDrought();
		response.setDescription("Number of drougth days");
		response.setNumberOfDays(number);
		return response;
	}
	
	@RequestMapping(value = "/rainy-periods", method = RequestMethod.GET)
	public WeatherResponseDto getRainyPeriods() {
		WeatherResponseDto response = new WeatherResponseDto();
		int number = solarSystemService.getRainyDays();
		response.setDescription("Number of rainy days");
		response.setNumberOfDays(number);
		return response;
	}
	
	@RequestMapping(value = "/maximum-rainy-days", method = RequestMethod.GET)
	public WeatherResponseDto getMaximumRainyDay() {
		WeatherResponseDto response = new WeatherResponseDto();
		int number = solarSystemService.getDayOfMaxRainy();
		response.setDescription("Day of maximum rain");
		response.setNumberOfDays(number);
		return response;
	}
	
	@RequestMapping(value = "/optimal-condition-periods", method = RequestMethod.GET)
	public WeatherResponseDto getOptimalCondition() {
		WeatherResponseDto response = new WeatherResponseDto();
		int number = solarSystemService.getOptimalConditionsDays();
		response.setDescription("Number of optimal condition days");
		response.setNumberOfDays(number);
		return response;
	}
}
