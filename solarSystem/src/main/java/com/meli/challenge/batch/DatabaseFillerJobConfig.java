package com.meli.challenge.batch;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.meli.challenge.config.ConstantsConfig;
import com.meli.challenge.model.Planet;
import com.meli.challenge.service.SolarSystemService;

@Configuration
public class DatabaseFillerJobConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(DatabaseFillerJobConfig.class);
	
	@Autowired
	private SolarSystemService solarSystemService;

	@Autowired
	private ConstantsConfig constantsConfig;

	private Planet ferengi;
	private Planet vulcano;
	private Planet betasoide;
	
	@Autowired
    private JobBuilderFactory jobBuilders;

    @Autowired
    private StepBuilderFactory stepBuilders;
    
    @Bean
    public Job databaseFillerJob() {
        return jobBuilders.get("databaseFillerJob")
            .start(taskletStep())
            .build();
    }
    
    @Bean
    public Step taskletStep() {
        return stepBuilders.get("taskletStep")
            .tasklet(tasklet())
            .build();
    }

    @Bean
    public Tasklet tasklet() {
        return (contribution, chunkContext) -> {
        	ferengi = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getFerengiSpeed(), constantsConfig.getFerengiRadius(), constantsConfig.getFerengiName());
			vulcano = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getVulcanoSpeed(), constantsConfig.getVulcanoRadius(), constantsConfig.getVulcanoName());
			betasoide = new Planet(constantsConfig.getInitialAngle(), constantsConfig.getBetasoideSpeed(), constantsConfig.getBetasoideRadius(), constantsConfig.getBetasoideName());
			List<Planet> planets = new ArrayList<>();
			planets.add(ferengi);
			planets.add(vulcano);
			planets.add(betasoide);
			solarSystemService.saveWeather(planets, constantsConfig.getDaysToIterate());
			logger.info("The planets has been generated: {}", planets);
            return RepeatStatus.FINISHED;
        };
    }

}
