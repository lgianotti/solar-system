package com.meli.challenge.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meli.challenge.model.Weather;

@Repository("weatherRepository")
public interface WeatherRepository extends JpaRepository<Weather, Long>{

}
