package com.meli.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meli.challenge.model.MaxRainyDay;

@Repository("maxRainyDayRepository")
public interface MaxRainyDayRepository extends JpaRepository<MaxRainyDay, Long>{

}
