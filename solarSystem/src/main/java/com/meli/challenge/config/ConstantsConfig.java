package com.meli.challenge.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "solar.system")
public class ConstantsConfig {
	
	private int daysToIterate;
	private double ferengiRadius;
	private double vulcanoRadius;
	private double betasoideRadius;
	private int ferengiSpeed;
	private int vulcanoSpeed;
	private int betasoideSpeed;
	private String errorMessage;
	private int minimumSizeLine;
	private int minimumSizeTriangle;
	private String betasoideName;
	private String vulcanoName;
	private String ferengiName;
	private String drougthDescription;
	private String rainyDescription;
	private String optimalDescription;
	private String notEspecificDescription;
	private int initialAngle;
	private String title;
	private String description;
	private String version;
	private String name;
	private String email;
	private String url;
	private double planteARadius;
	private double planteBRadius;
	private double planteCRadius;
	private int planteAAngle;
	private int planteBAngle;
	private int planteCAngle;
	private int nullSpeed;
	

	public int getDaysToIterate() {
		return daysToIterate;
	}

	public void setDaysToIterate(int daysToIterate) {
		this.daysToIterate = daysToIterate;
	}

	public double getFerengiRadius() {
		return ferengiRadius;
	}

	public void setFerengiRadius(double ferengiRadius) {
		this.ferengiRadius = ferengiRadius;
	}

	public double getVulcanoRadius() {
		return vulcanoRadius;
	}

	public void setVulcanoRadius(double vulcanoRadius) {
		this.vulcanoRadius = vulcanoRadius;
	}

	public double getBetasoideRadius() {
		return betasoideRadius;
	}

	public void setBetasoideRadius(double betasoideRadius) {
		this.betasoideRadius = betasoideRadius;
	}

	public int getFerengiSpeed() {
		return ferengiSpeed;
	}

	public void setFerengiSpeed(int ferengiSpeed) {
		this.ferengiSpeed = ferengiSpeed;
	}

	public int getVulcanoSpeed() {
		return vulcanoSpeed;
	}

	public void setVulcanoSpeed(int vulcanoSpeed) {
		this.vulcanoSpeed = vulcanoSpeed;
	}

	public int getBetasoideSpeed() {
		return betasoideSpeed;
	}

	public void setBetasoideSpeed(int betasoideSpeed) {
		this.betasoideSpeed = betasoideSpeed;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getMinimumSizeLine() {
		return minimumSizeLine;
	}

	public void setMinimumSizeLine(int minimumSizeLine) {
		this.minimumSizeLine = minimumSizeLine;
	}

	public int getMinimumSizeTriangle() {
		return minimumSizeTriangle;
	}

	public void setMinimumSizeTriangle(int minimumSizeTriangle) {
		this.minimumSizeTriangle = minimumSizeTriangle;
	}

	public String getBetasoideName() {
		return betasoideName;
	}

	public void setBetasoideName(String betasoideName) {
		this.betasoideName = betasoideName;
	}

	public String getVulcanoName() {
		return vulcanoName;
	}

	public void setVulcanoName(String vulcanoName) {
		this.vulcanoName = vulcanoName;
	}

	public String getFerengiName() {
		return ferengiName;
	}

	public void setFerengiName(String ferengiName) {
		this.ferengiName = ferengiName;
	}

	public String getDrougthDescription() {
		return drougthDescription;
	}

	public void setDrougthDescription(String drougthDescription) {
		this.drougthDescription = drougthDescription;
	}

	public String getRainyDescription() {
		return rainyDescription;
	}

	public void setRainyDescription(String rainyDescription) {
		this.rainyDescription = rainyDescription;
	}

	public String getOptimalDescription() {
		return optimalDescription;
	}

	public void setOptimalDescription(String optimalDescription) {
		this.optimalDescription = optimalDescription;
	}

	public String getNotEspecificDescription() {
		return notEspecificDescription;
	}

	public void setNotEspecificDescription(String notEspecificDescription) {
		this.notEspecificDescription = notEspecificDescription;
	}

	public int getInitialAngle() {
		return initialAngle;
	}

	public void setInitialAngle(int initialAngle) {
		this.initialAngle = initialAngle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getPlanteARadius() {
		return planteARadius;
	}

	public void setPlanteARadius(double planteARadius) {
		this.planteARadius = planteARadius;
	}

	public double getPlanteBRadius() {
		return planteBRadius;
	}

	public void setPlanteBRadius(double planteBRadius) {
		this.planteBRadius = planteBRadius;
	}

	public double getPlanteCRadius() {
		return planteCRadius;
	}

	public void setPlanteCRadius(double planteCRadius) {
		this.planteCRadius = planteCRadius;
	}

	public int getPlanteAAngle() {
		return planteAAngle;
	}

	public void setPlanteAAngle(int planteAAngle) {
		this.planteAAngle = planteAAngle;
	}

	public int getPlanteBAngle() {
		return planteBAngle;
	}

	public void setPlanteBAngle(int planteBAngle) {
		this.planteBAngle = planteBAngle;
	}

	public int getPlanteCAngle() {
		return planteCAngle;
	}

	public void setPlanteCAngle(int planteCAngle) {
		this.planteCAngle = planteCAngle;
	}

	public int getNullSpeed() {
		return nullSpeed;
	}

	public void setNullSpeed(int nullSpeed) {
		this.nullSpeed = nullSpeed;
	}
}
