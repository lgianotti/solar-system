package com.meli.challenge.service.impl;

import java.util.List;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meli.challenge.bussiness.SolarSystemBussiness;
import com.meli.challenge.config.ConstantsConfig;
import com.meli.challenge.exception.DayNotFoundException;
import com.meli.challenge.model.MaxRainyDay;
import com.meli.challenge.model.Planet;
import com.meli.challenge.model.Sun;
import com.meli.challenge.model.Weather;
import com.meli.challenge.repository.MaxRainyDayRepository;
import com.meli.challenge.repository.WeatherRepository;
import com.meli.challenge.service.SolarSystemService;

@Service
public class SolarSystemServiceImpl implements SolarSystemService {

	private static final Logger logger = LoggerFactory.getLogger(SolarSystemServiceImpl.class);

	private static List<Weather> weathers;

	private static int dayOfMaxRain;

	@Autowired
	private SolarSystemBussiness solarSystemBussiness;

	@Autowired
	private MaxRainyDayRepository maxRainyDayRepository;

	@Autowired
	private WeatherRepository weatherRepository;

	@Autowired
	private ConstantsConfig constantConfig;

	@Override
	public boolean areAlign(int day, List<Planet> planets) {
		logger.info("class:SolarSystemServiceImpl, method:areAlign, day:{}, planets:{}", day, planets);
		return solarSystemBussiness.areAlign(day, planets);
	}

	@Override
	public boolean areAlign(int day, List<Planet> planets, Sun sun) {
		logger.info("class:SolarSystemServiceImpl, method:areAlign, day:{}, planets:{}", day, planets);
		return solarSystemBussiness.areAlign(day, planets, sun);
	}

	@Override
	public boolean triangleContainsSun(int days, List<Planet> planets) {
		logger.info("class:SolarSystemServiceImpl, method:areAlign, day:{}, planets:{}", days, planets);
		return solarSystemBussiness.triangleContainsSun(days, planets);
	}

	@Override
	public double getPerimeter(int day, List<Planet> planets) {
		logger.info("class:SolarSystemServiceImpl, method:getPerimeter, day:{}, planets:{}", day, planets);
		return solarSystemBussiness.perimeter(day, planets);
	}

	@Override
	public List<Integer> getnumberOfPeriodsOfDrought(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemServiceImpl, method:getnumberOfPeriodsOfDrought, days:{}, planets:{}",
				numberOfDays, planets);
		return solarSystemBussiness.getNumberOfPeriodsOfDrought(planets, numberOfDays);
	}

	@Override
	public List<Integer> getRainyDays(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemServiceImpl, method:getRainyDays, day:{}, planets:{}", numberOfDays, planets);
		return solarSystemBussiness.getRainyDays(planets, numberOfDays);
	}

	@Override
	public int getDayOfMaxRainy(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemServiceImpl, method:getDayOfMaxRainy, day:{}, planets:{}", numberOfDays, planets);
		return solarSystemBussiness.getDayOfMaxRainy(planets, numberOfDays);
	}

	@Override
	public List<Integer> getOptimalConditionsDays(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemServiceImpl, method:getOptimalConditionsDays, day:{}, planets:{}", numberOfDays,
				planets);
		return solarSystemBussiness.getOptimalConditionsDays(planets, numberOfDays);
	}

	@Override
	public List<Weather> getWeathers(List<Planet> planets) {
		logger.info("class:getWeathers, method:areAlign, planets:{}", planets);
		if (weathers != null && !weathers.isEmpty()) {
			return weathers;
		}else {
			weathers = weatherRepository.findAll();
			return weathers;
		}
	}

	@Override
	public void saveWeather(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemServiceImpl, method:saveWeather, day:{}, planets:{}", numberOfDays, planets);
		List<Weather> weathers = solarSystemBussiness.generateDataForWeather(planets, numberOfDays);
		weatherRepository.saveAll(weathers);
		MaxRainyDay max = new MaxRainyDay();
		max.setDayOfMaxRain(solarSystemBussiness.getDayOfMaxRainy(planets, numberOfDays));
		maxRainyDayRepository.save(max);
	}

	@Override
	public Weather getWeatherByDay(Long day) throws DayNotFoundException {
		logger.info("class:SolarSystemServiceImpl, method:getWeatherByDay, day:{}", day);
		return weatherRepository.findById(day).orElseThrow(new Supplier<DayNotFoundException>() {

			@Override
			public DayNotFoundException get() {
				return new DayNotFoundException();
			}
		});
	}

	@Override
	public int getnumberOfPeriodsOfDrought() {
		logger.info("class:SolarSystemServiceImpl, method:getnumberOfPeriodsOfDrought");
		int number = (int) weatherRepository.findAll().stream()
				.filter(wheater -> wheater.isRainy(constantConfig.getDrougthDescription())).count();
		return number;
	}

	@Override
	public int getRainyDays() {
		logger.info("class:SolarSystemServiceImpl, method:getRainyDays");
		int number = (int) weatherRepository.findAll().stream()
				.filter(wheater -> wheater.isRainy(constantConfig.getRainyDescription())).count();
		return number;
	}

	@Override
	public int getDayOfMaxRainy() {
		logger.info("class:SolarSystemServiceImpl, method:getDayOfMaxRainy");
		MaxRainyDay max = maxRainyDayRepository.findAll().get(0);
		return max.getDayOfMaxRain();
	}

	@Override
	public int getOptimalConditionsDays() {
		logger.info("class:SolarSystemServiceImpl, method:getOptimalConditionsDays");
		if (dayOfMaxRain == 0) {
			dayOfMaxRain = (int) weatherRepository.findAll().stream()
					.filter(wheater -> wheater.isRainy(constantConfig.getOptimalDescription())).count();
		}
		return dayOfMaxRain;
	}

}
