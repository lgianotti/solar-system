package com.meli.challenge.service;

import java.util.List;

import com.meli.challenge.exception.DayNotFoundException;
import com.meli.challenge.model.Planet;
import com.meli.challenge.model.Sun;
import com.meli.challenge.model.Weather;

public interface SolarSystemService {
	boolean areAlign(int day, List<Planet> planets);
	boolean areAlign(int day, List<Planet> planets, Sun sun);
	boolean triangleContainsSun(int days, List<Planet> planets);
	double getPerimeter(int day,  List<Planet> planets);
	List<Integer> getnumberOfPeriodsOfDrought(List<Planet> planets, int numberOfDays);
	List<Integer> getRainyDays(List<Planet> planets, int numberOfDays);
	int getDayOfMaxRainy(List<Planet> planets, int numberOfDays);
	List<Integer> getOptimalConditionsDays(List<Planet> planets, int numberOfDays);
	List<Weather> getWeathers(List<Planet> planets);
	void saveWeather(List<Planet> planets, int numberOfDays);
	Weather getWeatherByDay(Long day) throws DayNotFoundException;
	int getnumberOfPeriodsOfDrought();
	int getRainyDays();
	int getDayOfMaxRainy();
	int getOptimalConditionsDays();
}
