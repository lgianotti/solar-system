package com.meli.challenge.bussiness;

import java.awt.geom.Point2D;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.meli.challenge.model.Planet;
import com.meli.challenge.util.SolarSystemUtil;

@Component(value="alignVectorMethod")
@Primary
public class AlignVectorMethod extends Alignable{
	
	private static final Logger logger = LoggerFactory.getLogger(AlignVectorMethod.class);

	@Override
	public boolean areAlign(int day, List<Planet> planets) {
		logger.info("class:AlignVectorMethod, method:areAlign, day:{}, planets:{}", day, planets);
		boolean ok = true;
		Planet firstPlanet = planets.get(0);
		for (int i = 1; i < planets.size() - 1; i++) {
			if (i + 1 <= planets.size() - 1) {
				Point2D.Double vectorDirector1 = getVectorDirector(firstPlanet.getCoordinates(day),
						planets.get(i).getCoordinates(day));
				Point2D.Double vectorDirector2 = getVectorDirector(firstPlanet.getCoordinates(day),
						planets.get(i + 1).getCoordinates(day));

				vectorDirector1 = SolarSystemUtil.roundCoordinate(vectorDirector1);
				vectorDirector2 = SolarSystemUtil.roundCoordinate(vectorDirector2);

				if (!vectorAlign(vectorDirector1, vectorDirector2)) {
					ok = false;
					break;
				}
			}
		}
		return ok;
	}
}
