package com.meli.challenge.bussiness;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meli.challenge.model.CelestialBody;
import com.meli.challenge.model.Planet;
import com.meli.challenge.util.SolarSystemUtil;

public abstract class Alignable {
	
	private static final Logger logger = LoggerFactory.getLogger(Alignable.class);
	
	public abstract boolean areAlign(int day, List<Planet> planets);

	protected boolean areAlign(int day, List<Planet> planets, CelestialBody sun) {
		logger.info("class:Alignable, method:areAlign, day:{}, planets:{}", day, planets);
		boolean ok = true;
		for (int i = 0; i < planets.size() - 1; i++) {
			if (i + 1 <= planets.size() - 1) {

				Point2D.Double vectorDirector1 = getVectorDirector(sun.getCoordinates(day),
						planets.get(i).getCoordinates(day));
				Point2D.Double vectorDirector2 = getVectorDirector(sun.getCoordinates(day),
						planets.get(i + 1).getCoordinates(day));

				vectorDirector1 = SolarSystemUtil.roundCoordinate(vectorDirector1);
				vectorDirector2 = SolarSystemUtil.roundCoordinate(vectorDirector2);

				if (!vectorAlign(vectorDirector1, vectorDirector2)) {
					ok = false;
					break;
				}
			}
		}
		return ok;
	}

	protected Point2D.Double getVectorDirector(Point2D.Double coorinate1, Point2D.Double coordinate2) {
		logger.info("class:Alignable, method:getVectorDirector, coorinate1:{}, coordinate2:{}", coorinate1, coordinate2);
		double vx = SolarSystemUtil.round(2,coordinate2.getX()).add(SolarSystemUtil.round(2, -1 * coorinate1.getX())).doubleValue();
		double vy = SolarSystemUtil.round(2,coordinate2.getY()).add(SolarSystemUtil.round(2, -1 * coorinate1.getY())).doubleValue();
		return new Point2D.Double(vx, vy);
	}

	protected boolean vectorAlign(Point2D.Double vectorDirector1, Point2D.Double vectorDirector2) {
		logger.info("class:Alignable, method:vectorAlign, coorinate1:{}, coordinate2:{}", vectorDirector1, vectorDirector2);
		BigDecimal x1y2 = SolarSystemUtil.round(2,vectorDirector1.getX()).multiply(SolarSystemUtil.round(2,vectorDirector2.getY()));
		BigDecimal x2y1 = SolarSystemUtil.round(2,vectorDirector2.getX()).multiply(SolarSystemUtil.round(2,vectorDirector1.getY()));
		return x1y2.subtract(x2y1).compareTo(BigDecimal.ZERO) == 0;
	}

}
