package com.meli.challenge.bussiness;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meli.challenge.config.ConstantsConfig;
import com.meli.challenge.model.CelestialBody;
import com.meli.challenge.model.Planet;
import com.meli.challenge.model.Sun;
import com.meli.challenge.model.Weather;
import com.meli.challenge.util.SolarSystemUtil;

@Service
public class SolarSystemBussiness {
	
	private static final Logger logger = LoggerFactory.getLogger(SolarSystemBussiness.class);
	
	@Autowired
	private ConstantsConfig constantConfig;
	
	@Autowired
	private Alignable alignMethod;
	
	public boolean areAlign(int day, List<Planet> planets) {
		logger.info("class:SolarSystemBussiness, method:areAlign, day:{}, planets:{}", day, planets);
		if (planets.size() > constantConfig.getMinimumSizeLine()) {
			return alignMethod.areAlign(day, planets);
		} else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}
	}

	public boolean areAlign(int day, List<Planet> planets, CelestialBody sun) {
		logger.info("class:SolarSystemBussiness, method:areAlign, day:{}, planets:{}", day, planets);
		if (planets.size() > constantConfig.getMinimumSizeLine()) {
			return alignMethod.areAlign(day, planets, sun);
		}else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}
	}

	public boolean triangleContainsSun(int days, List<Planet> planets) {
		logger.info("class:SolarSystemBussiness, method:triangleContainsSun, day:{}, planets:{}", days, planets);
		if (planets.size() != constantConfig.getMinimumSizeTriangle()) {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		} else if (!areAlign(days, planets)) {

			Point2D.Double p1 = planets.get(0).getCoordinates(days);
			BigDecimal p1x = new BigDecimal(p1.getX());
			BigDecimal p1y = new BigDecimal(p1.getY());

			Point2D.Double p2 = planets.get(1).getCoordinates(days);
			BigDecimal p2x = new BigDecimal(p2.getX());
			BigDecimal p2y = new BigDecimal(p2.getY());

			Point2D.Double p3 = planets.get(2).getCoordinates(days);
			BigDecimal p3x = new BigDecimal(p3.getX());
			BigDecimal p3y = new BigDecimal(p3.getY());

			// A1A2A3 (A1.x - A3.x) * (A2.y - A3.y) - (A1.y - A3.y) * (A2.x - A3.x)
			BigDecimal firstSubstraend = SolarSystemUtil.round(2, (p1x.subtract(p3x)).multiply((p2y.subtract(p3y))));
			BigDecimal secondSubstraend = SolarSystemUtil.round(2,(p1y.subtract(p3y)).multiply((p2x.subtract(p3x))));
			BigDecimal firstResult = SolarSystemUtil.round(2,firstSubstraend.subtract(secondSubstraend));

			// A1A2P (A1.x * A2.y) - (A1.y * A2.x)
			firstSubstraend = SolarSystemUtil.round(2,p1x.multiply(p2y));

			secondSubstraend = SolarSystemUtil.round(2,p1y.multiply(p2x));

			BigDecimal secondResult = SolarSystemUtil.round(2,firstSubstraend.subtract(secondSubstraend));

			// A2A3P (A2.x * A3.y) - (A2.y * A3.x)

			firstSubstraend = SolarSystemUtil.round(2,p2x.multiply(p3y));

			secondSubstraend = SolarSystemUtil.round(2,p2y.multiply(p3x));

			BigDecimal thirdResult = SolarSystemUtil.round(2,firstSubstraend.subtract(secondSubstraend));

			// A3A1P (A3.x * A1.y) - (A3.y * A1.x)
			firstSubstraend = SolarSystemUtil.round(2,p3x.multiply(p1y));

			secondSubstraend = SolarSystemUtil.round(2,p3y.multiply(p1x));

			BigDecimal fourthResult = SolarSystemUtil.round(2,firstSubstraend.subtract(secondSubstraend));

			boolean conditionOne = firstResult.compareTo(BigDecimal.ZERO) >= 0
					&& secondResult.compareTo(BigDecimal.ZERO) >= 0 && thirdResult.compareTo(BigDecimal.ZERO) >= 0
					&& fourthResult.compareTo(BigDecimal.ZERO) >= 0;

			boolean conditionTwo = firstResult.compareTo(BigDecimal.ZERO) < 0
					&& secondResult.compareTo(BigDecimal.ZERO) < 0 && thirdResult.compareTo(BigDecimal.ZERO) < 0
					&& fourthResult.compareTo(BigDecimal.ZERO) < 0;

			return conditionOne || conditionTwo;
		} else {
			return false;
		}
	}

	public double perimeter(int day, List<Planet> planets) {
		logger.info("class:SolarSystemBussiness, method:perimeter, day:{}, planets:{}", day, planets);
		if (planets.size() > constantConfig.getMinimumSizeLine()) {
			Point2D.Double p1Coordenate = planets.get(0).getCoordinates(day);
			Point2D.Double p2Coordenate = planets.get(1).getCoordinates(day);
			Point2D.Double p3Coordenate = planets.get(2).getCoordinates(day);

			double xValue = p1Coordenate.getX() - p2Coordenate.getX();
			double yValue = p1Coordenate.getY() - p2Coordenate.getY();

			double d1 = Math.hypot(xValue, yValue);

			xValue = p1Coordenate.getX() - p3Coordenate.getX();
			yValue = p1Coordenate.getY() - p3Coordenate.getY();

			double d2 = Math.hypot(xValue, yValue);

			xValue = p2Coordenate.getX() - p3Coordenate.getX();
			yValue = p2Coordenate.getY() - p3Coordenate.getY();

			double d3 = Math.hypot(xValue, yValue);

			return SolarSystemUtil.round(2, d1 + d2 + d3).doubleValue();
		} else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}
	}

	public List<Integer> getNumberOfPeriodsOfDrought(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemBussiness, method:getNumberOfPeriodsOfDrought, day:{}, planets:{}", numberOfDays, planets);
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i < numberOfDays; i++) {

			if (areAlign(i, planets) && areAlign(i, planets, new Sun())) {
				days.add(i);
			}
		}
		return days;
	}

	public List<Integer> getRainyDays(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemBussiness, method:getRainyDays, day:{}, planets:{}", numberOfDays, planets);
		if (planets.size() > constantConfig.getMinimumSizeLine()) {
			List<Integer> rainyDays = new ArrayList<>();
			for (int i = 0; i <= numberOfDays; i++) {
				boolean isTriangle = triangleContainsSun(i, planets);
				if (isTriangle) {
					rainyDays.add(i);
				}
			}
			return rainyDays;
		} else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}
	}

	public int getDayOfMaxRainy(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemBussiness, method:getDayOfMaxRainy, day:{}, planets:{}", numberOfDays, planets);
		if (planets.size() == constantConfig.getMinimumSizeTriangle()) {
			double maxperimeter = 0;
			int dayToReturn = 0;
			for (int i = 0; i <= numberOfDays; i++) {
				boolean isTriangle = triangleContainsSun(i, planets);
				if (isTriangle) {
					double perimeter = perimeter(i, planets);
					if (perimeter > maxperimeter) {
						dayToReturn = i;
					}
				}
			}
			return dayToReturn;
		} else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}

	}

	public List<Integer> getOptimalConditionsDays(List<Planet> planets, int numberOfDays) {
		logger.info("class:SolarSystemBussiness, method:getOptimalConditionsDays, day:{}, planets:{}", numberOfDays, planets);
		if (planets.size() > constantConfig.getMinimumSizeLine()) {
			List<Integer> days = new ArrayList<>();
			List<Integer> daysAlignmentWithSun = listOfDaysAlignment(planets, numberOfDays, new Sun());
			List<Integer> daysAlignmentWithOutSun = listOfDaysAlignment(planets, numberOfDays);

			days = intersection(daysAlignmentWithSun, daysAlignmentWithOutSun);
			return days;
		} else {
			throw new IllegalArgumentException(constantConfig.getErrorMessage());
		}
	}

	private List<Integer> listOfDaysAlignment(List<Planet> planets, int day) {
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i <= day; i++) {
			if (areAlign(i, planets)) {
				days.add(i);
			}
		}
		return days;
	}

	private List<Integer> listOfDaysAlignment(List<Planet> planets, int day, CelestialBody sun) {
		List<Integer> days = new ArrayList<>();
		for (int i = 0; i <= day; i++) {
			if (areAlign(i, planets, sun)) {
				days.add(i);
			}
		}
		return days;
	}

	private <T> List<T> intersection(List<T> list1, List<T> list2) {
		List<T> list = new ArrayList<T>();

		for (T t : list1) {
			if (list2.contains(t)) {
				list.add(t);
			}
		}
		return list;
	}

	public List<Weather> generateDataForWeather(List<Planet> planets, int days) {
		logger.info("class:SolarSystemBussiness, method:generateDataForWeather, day:{}, planets:{}", days, planets);
		List<Weather> wheathers = new ArrayList<>();
		for (int i = 0; i <= days; i++) {
			Weather w = new Weather();
			w.setDay(Long.valueOf(i));
			if (areAlign(i, planets)) {
				if (areAlign(i, planets, new Sun())) {
					w.setWeather(constantConfig.getDrougthDescription());
				} else {
					w.setWeather(constantConfig.getOptimalDescription());
				}

			} else if (triangleContainsSun(i, planets)) {
				w.setWeather(constantConfig.getRainyDescription());
			} else {
				w.setWeather(constantConfig.getNotEspecificDescription());
			}
			wheathers.add(w);
		}
		return wheathers;
	}

}
