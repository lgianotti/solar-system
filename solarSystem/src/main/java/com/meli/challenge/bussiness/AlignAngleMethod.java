package com.meli.challenge.bussiness;

import java.awt.geom.Point2D;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.meli.challenge.model.Planet;

@Component(value = "alignAngleMethod")
public class AlignAngleMethod extends Alignable {
	
	private static final Logger logger = LoggerFactory.getLogger(AlignAngleMethod.class);

	@Override
	public boolean areAlign(int day, List<Planet> planets) {
		logger.info("class:AlignAngleMethod, method:areAlign, day:{}, planets:{}", day, planets);
		boolean ok = true;
		Planet firstPlanet = planets.get(0);
		for (int i = 1; i < planets.size() - 1; i++) {
			if (i + 1 <= planets.size() - 1) {
				Point2D.Double v2 = getVectorDirector(firstPlanet.getCoordinates(day),
						planets.get(i).getCoordinates(day));
				Point2D.Double v3 = getVectorDirector(firstPlanet.getCoordinates(day),
						planets.get(i + 1).getCoordinates(day));

				Double angleV2 = getAngleWithAxis(v2);
				Double angleV3 = getAngleWithAxis(v3);

				return (angleV2.compareTo(angleV3) == 0 || angleV2.compareTo(angleV3 + 180) == 0
						|| angleV2.compareTo(angleV3 - 180) == 0);
			}
		}
		return ok;

	}

	private Double getAngleWithAxis(Point2D.Double vector) {
		logger.info("class:AlignAngleMethod, method:getAngleWithAxis, vector:{}", vector);
		Double ret = new Double(0);
		if (vector.getX() != 0D) {
			ret = Math.atan(vector.getY() / vector.getX());
		}
		return ret;
	}

}
