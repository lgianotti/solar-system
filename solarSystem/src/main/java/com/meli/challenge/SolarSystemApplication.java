package com.meli.challenge;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableBatchProcessing
@SpringBootApplication
public class SolarSystemApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SolarSystemApplication.class, args);
	}
}
