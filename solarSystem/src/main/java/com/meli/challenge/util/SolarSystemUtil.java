package com.meli.challenge.util;

import java.awt.geom.Point2D;
import java.math.BigDecimal;

public class SolarSystemUtil {

	public static Point2D.Double roundCoordinate(Point2D.Double coordinate) {
		double x = round(2, coordinate.getX()).doubleValue();
		double y = round(2, coordinate.getY()).doubleValue();
		return new Point2D.Double(x, y);
	}
	
	public static BigDecimal round(int scale, double number) {
		return new BigDecimal(number).setScale(scale, BigDecimal.ROUND_HALF_UP);
	}
	
	public static BigDecimal round(int scale, BigDecimal number) {
		return number.setScale(scale, BigDecimal.ROUND_HALF_UP);
	}
}
