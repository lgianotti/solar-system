package com.meli.challenge.dto;

import java.util.List;

public class WeatherRequestDto {
	private long days;
	private List<PlanetRequestDto> planets;
	
	public long getDays() {
		return days;
	}
	public void setDays(long days) {
		this.days = days;
	}
	public List<PlanetRequestDto> getPlanets() {
		return planets;
	}
	public void setPlanets(List<PlanetRequestDto> planets) {
		this.planets = planets;
	}
}
